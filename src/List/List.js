import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';

import Panel from '../InfoPanel/InfoPanel';
import * as actions from '../actions';

import './List.scss';

class List extends Component {

    componentDidMount() {
        this.props.dispatch(fetchData(this.props.seed, this.props.results));
        // const results = this.props.results ? this.props.results : Math.floor(Math.random() * 8) + 2;
        // const seed = this.props.seed ? this.props.seed : null;
        // axios.get('https://randomuser.me/api', {
        //     params: {
        //         results: results,
        //         seed: seed
        //     }
        // }).then(res => {
        //     this.props.onSeed(res.data.info.seed, results);
        //     this.setState({
        //         users: res.data.results,
        //         info: res.data.info
        //     });
        // });
    }

    render() {
        console.log('state', this);
        if (!this.props.users || !this.props.users.length) {
            return <div>Loading</div>
        }
        return (
            <div className="List">
                {this.props.users.map((usr, idx) => (
                    <Panel
                        id={idx + ':' + this.props.seed}
                        key={usr.login.uuid}
                        name={usr.name.first + ' ' + usr.name.last} />))}
            </div>
        );
    }
}

const setData = (results, data) => ({ type: actions.SET_DATA, data, results });

const fetchData = (seed, res) => dispach => {
    const results = res || Math.floor(Math.random() * 8) + 2;
    axios.get('https://randomuser.me/api', { params: { results, seed } })
        .then(res => dispach(setData(results, res.data)));
};

const mapStateToProps = state => { console.log('map', state); return ({ seed: state.seed, results: state.results, users: state.users }) };

export default connect(mapStateToProps)(List);

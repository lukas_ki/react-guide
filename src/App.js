import React, { Component } from 'react';
import { Route, NavLink } from 'react-router-dom';

import List from './List/List';
import Details from './Details/Details';

import './App.scss';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header>
          <NavLink to="/" exact>Home</NavLink>
          <a href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer">
            Learn React
          </a>
        </header>
        <Route path="/" exact component={List} />
        <Route path="/:id" exact component={Details} />
      </div>
    );
  }
}

export default App;

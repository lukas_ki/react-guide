import React, { Component } from 'react';
import axios from 'axios';

import Panel from '../InfoPanel/InfoPanel';

import './Details.scss';

class Details extends Component {

    state = {};

    componentDidMount() {
        const [id, seed] = this.props.match.params.id.split(':');
        axios.get('https://randomuser.me/api', {
            params: {
                seed: seed,
                results: +id + 1
            }
        }).then(res => {
            this.setState({
                usr: res.data.results[+id],
                info: res.data.info
            });
        });
    }

    render() {
        return this.state.usr ? (
            <Panel
                key={this.state.usr.login.uuid}
                name={this.state.usr.name.first + ' ' + this.state.usr.name.last}>{this.state.usr.email}</Panel>) : null;
    }
}

export default Details;

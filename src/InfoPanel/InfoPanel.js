import React from 'react';
import { Link } from 'react-router-dom';

import './InfoPanel.scss';

const panel = (props) => {
    let desc;
    if (props.children) {
        desc = <p>{props.children}</p>
    }
    let name = props.name;
    if (props.id) {
        name = React.createElement(Link, { to: props.id }, name);
    }
    return React.createElement('div', { className: 'Panel' },
        React.createElement('h1', null, name),
        desc);
}

export default panel;

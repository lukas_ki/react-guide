import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, compose, combineReducers, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import thunk from 'redux-thunk';

import App from './App';
import * as actions from './actions';
import * as serviceWorker from './serviceWorker';

import './index.css';

const reducer = (state = {}, action) => {
    console.log('reduce', action);
    switch (action.type) {
        case actions.UPDATE_SEED: return { ...state, seed: action.seed, results: action.results };
        case actions.SET_DATA: return { ...state, seed: action.data.info.seed, users: action.data.results, results: action.results };
        default: return state;
    }
}

const logger = store => next => action => {
    const result = next(action);
    console.log('[Middleware] next state', store.getState(), result);
    return result;
}

ReactDOM.render((
    <Provider store={createStore(reducer, applyMiddleware(thunk))}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>
), document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
